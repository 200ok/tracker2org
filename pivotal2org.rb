require 'rest-client'
require 'json'
require 'yaml'

# Change into the current directory
Dir.chdir File.dirname(__FILE__)

config = YAML.load_file('config.yml')

org_todos = []
owner = config['pivotal']['username']
pivotal_api_token = config['pivotal']['token']
pivotal_todos_path = config['pivotal_todos_path']

config['projects'].each do |project|
  project_name = project[0]
  project_id = project[1]

  url = "https://www.pivotaltracker.com/services/v5/projects/#{project_id}/stories?filter=owner:#{owner} state:unstarted"

  entries = RestClient.get url,
    'Content-Type' => 'application/json',
    'X-TrackerToken' => pivotal_api_token

  entries = JSON.parse entries

  org_todos << "* PROJECT #{project_name}"

  entries.map do |entry|
    org_todos << "** TODO #{entry['name']}"
    org_todos << "Link to story: [[https://www.pivotaltracker.com/story/show/#{entry['id']}]]\n"
    org_todos << entry['description']
  end
end

File.write pivotal_todos_path, org_todos.join("\n")

diff = `cd #{File.dirname(pivotal_todos_path)}; git diff #{pivotal_todos_path}`
`notify-send "New Todos from Pivotal! #{diff}"` unless diff.empty?
